import {useCallback, useEffect, useState} from "react";
import {HubConnection} from "@microsoft/signalr";

type MapFn<TV, TR> = (v: TV) => TR;

export interface UseStreamResult<T> {
    value: T[];
    complete: boolean;
    error: never | null;
}

export default function useStream<TV, TR = TV>(
    signalR: HubConnection | null,
    name: string,
    map: MapFn<TV, TR>
): UseStreamResult<TR> {
    const [value, setValue] = useState<TR[]>([]);
    const [complete, setComplete] = useState(false);
    const [error, setError] = useState<never | null>(null);

    const handleNext = useCallback(
        (v: TV) => {
            const mapped = map(v);
            setValue(v => [...v, mapped]);
        },
        [map]
    );

    const handleComplete = useCallback(() => {
        setComplete(true);
    }, []);

    const handleError = useCallback((err: never) => {
        setError(err);
    }, []);

    useEffect(() => {
        if (!signalR) return;

        const subscription = signalR.stream<TV>(name).subscribe({
            next: handleNext,
            complete: handleComplete,
            error: handleError
        });

        return () => subscription.dispose();
    }, [signalR, name, handleNext, handleComplete, handleError]);

    return {value, complete, error};
}
