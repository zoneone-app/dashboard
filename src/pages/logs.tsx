import React, {FC} from "react";
import {useSignalR} from "../api/hook";
import {LogList, LogListItemProps} from "../components/LogList";
import {Container, Main} from "../components/Container";
import {DefaultSidebar} from "../components/DefaultSidebar";
import {DarkModeSwitch} from "../components/DarkModeSwitch";
import useStream from "../hooks/useStream";

const LogPage: FC = () => {
    const signalR = useSignalR();

    const {value: logs} = useStream<LogListItemProps>(
        signalR,
        "GetLogs",
        v => ({
            ...v,
            time: new Date(v.time)
        })
    );

    return <LogList>{logs}</LogList>;
};

const Logs: FC = () => (
    <Container>
        <DefaultSidebar current="/logs" />
        <Main>
            <LogPage />
        </Main>
        <DarkModeSwitch />
    </Container>
);

export default Logs;
