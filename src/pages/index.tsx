import React, {FC} from "react";
import {Container} from "../components/Container";
import {DarkModeSwitch} from "../components/DarkModeSwitch";
import {DefaultSidebar} from "../components/DefaultSidebar";

const Index: FC = () => (
    <Container>
        <DefaultSidebar current="/" />
        <DarkModeSwitch />
    </Container>
);

export default Index;
