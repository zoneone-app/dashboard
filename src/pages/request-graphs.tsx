import React, {FC, useEffect, useState, lazy, Suspense} from "react";
import {format} from "date-fns";
import {Container, Main} from "../components/Container";
import {DefaultSidebar} from "../components/DefaultSidebar";
import {DarkModeSwitch} from "../components/DarkModeSwitch";
import {useSignalR} from "../api/hook";

interface RequestCountInfo {
    time: string;
    count: number;
    medDurationMs: number;
}

const Chart =
    typeof window === "undefined"
        ? null
        : lazy(() => import("react-apexcharts"));

const baseConfig = {
    xaxis: {
        type: "datetime",
        labels: {
            formatter: (time: number) => format(new Date(time), "mm:ss")
        }
    },
    tooltip: {
        enabled: false
    },
    stroke: {
        curve: "smooth"
    }
};

const RequestGraphsPage: FC = () => {
    const signalR = useSignalR();
    const [data, setData] = useState<RequestCountInfo[]>([]);

    useEffect(() => {
        const interval = setInterval(async () => {
            const newItem = await signalR?.invoke<RequestCountInfo>(
                "GetRequestStatus"
            );
            if (!newItem) return;

            setData([...data.slice(-9), newItem]);
        }, 1000);

        return () => clearInterval(interval);
    });

    if (!Chart)
        return (
            <p>This page requires Javascript to show and update the graphs.</p>
        );

    return (
        <Suspense fallback={<p>Loading graph component...</p>}>
            <Chart
                series={[
                    {
                        data: data.map(v => [
                            new Date(v.time).getTime(),
                            Math.floor(v.count * 10) / 10
                        ])
                    }
                ]}
                options={{
                    ...baseConfig,
                    chart: {
                        id: "request-count",
                        type: "line",
                        toolbar: {show: false}
                    },
                    yaxis: {
                        title: {
                            text: "requests per second"
                        }
                    }
                }}
                height={300}
            />
            <Chart
                series={[
                    {
                        data: data.map(v => [
                            new Date(v.time).getTime(),
                            Math.floor(v.medDurationMs * 100) / 100
                        ])
                    }
                ]}
                options={{
                    ...baseConfig,
                    chart: {
                        id: "request-time",
                        type: "line",
                        toolbar: {show: false}
                    },
                    yaxis: {
                        title: {
                            text: "avg ms per request"
                        }
                    }
                }}
                height={300}
            />
        </Suspense>
    );
};

const RequestGraphs: FC = () => (
    <Container>
        <DefaultSidebar current="/request-graphs" />
        <Main>
            <RequestGraphsPage />
        </Main>
        <DarkModeSwitch />
    </Container>
);

export default RequestGraphs;
