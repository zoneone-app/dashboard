import {UrlObject} from "url";
import React, {FC, PropsWithChildren} from "react";
import {
    useColorModeValue,
    VStack,
    Heading,
    Text,
    TextProps,
    Link as ChakraLink
} from "@chakra-ui/react";
import PropTypes from "prop-types";
import Link from "next/link";

interface SidebarButtonProps {
    href: string | UrlObject;
}

export const SidebarButton: FC<
    PropsWithChildren<SidebarButtonProps>
> = props => {
    const hoverBgColour = useColorModeValue("#0002", "#fff2");

    return (
        <Link href={props.href} passHref>
            <ChakraLink
                display="block"
                width="100%"
                py={2}
                px={4}
                _hover={{background: hoverBgColour}}
            >
                {props.children}
            </ChakraLink>
        </Link>
    );
};

SidebarButton.propTypes = {
    href: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.shape({
            auth: PropTypes.string,
            hash: PropTypes.string,
            host: PropTypes.string,
            hostname: PropTypes.string,
            href: PropTypes.string,
            pathname: PropTypes.string,
            protocol: PropTypes.string,
            search: PropTypes.string,
            slashes: PropTypes.bool,
            port: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            query: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.objectOf(PropTypes.string)
            ])
        })
    ]).isRequired,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.string])
};

export const SidebarHeader: FC<TextProps> = props => (
    <Text
        py={1}
        px={4}
        borderBottom="1px solid currentColor"
        fontSize=".8em"
        css={{fontVariant: "small-caps"}}
        width="100%"
        {...props}
    />
);

export interface SidebarProps {
    title: string;
}

export const Sidebar: FC<PropsWithChildren<SidebarProps>> = props => {
    const bgColour = useColorModeValue("gray.200", "gray.700");

    return (
        <VStack spacing={4} py={4} background={bgColour}>
            <Heading size="md" px={4}>
                {props.title}
            </Heading>

            {props.children}
        </VStack>
    );
};

Sidebar.propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.arrayOf(PropTypes.element).isRequired
};
