import React, {FC} from "react";
import {Box, Flex, useColorModeValue, FlexProps} from "@chakra-ui/react";

export const Container: FC<FlexProps> = (props: FlexProps) => {
    const bgColor = useColorModeValue("gray.50", "gray.900");
    const color = useColorModeValue("black", "white");
    return (
        <Flex
            direction="row"
            bg={bgColor}
            color={color}
            height="100vh"
            {...props}
        />
    );
};

export const Main: FC = props => (
    <Box height="100vh" overflowY="auto" flexGrow={1}>
        {props.children}
    </Box>
);
