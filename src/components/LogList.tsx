import {
    Box,
    BoxProps,
    Center,
    Text,
    HStack,
    useColorModeValue
} from "@chakra-ui/react";
import React, {FC, useEffect, useRef} from "react";
import {LogLevel} from "@microsoft/signalr";
import {format} from "date-fns";

type LogLevelInfo = {
    [key in LogLevel]: {
        label: string;
        bg: string;
        color: string;
    };
};

const logLevelInfo: LogLevelInfo = {
    [LogLevel.None]: {
        label: "none",
        bg: "transparent",
        color: "currentColor"
    },
    [LogLevel.Critical]: {
        label: "crit",
        bg: "red",
        color: "white"
    },
    [LogLevel.Error]: {
        label: "errr",
        bg: "orange",
        color: "black"
    },
    [LogLevel.Warning]: {
        label: "warn",
        bg: "yellow",
        color: "black"
    },
    [LogLevel.Information]: {
        label: "info",
        bg: "white",
        color: "black"
    },
    [LogLevel.Debug]: {
        label: "debg",
        bg: "white",
        color: "green.700"
    },
    [LogLevel.Trace]: {
        label: "trce",
        bg: "white",
        color: "blue.500"
    }
};

export interface LogListItemProps {
    time: Date;
    level: LogLevel;
    event: {
        id: number;
        name: string;
    };
    name: string;
    message: string;
}

const LogListItem: FC<LogListItemProps> = props => {
    const evenBackground = useColorModeValue("#0001", "#fff1");
    const levelInfo = logLevelInfo[props.level];
    const date = format(props.time, "HH:mm:ss.SS");

    return (
        <HStack
            m={0}
            css={{
                ":nth-child(even)": {
                    background: evenBackground
                }
            }}
        >
            <Box ml={2} fontFamily="mono" fontSize=".8rem">
                {date}
            </Box>
            <Box
                background={levelInfo.bg}
                color={levelInfo.color}
                p={2}
                alignSelf="stretch"
                fontFamily="mono"
                fontSize=".8rem"
            >
                {levelInfo.label}
            </Box>
            <Box opacity={0.8}>{props.name}:</Box>
            <Box flexGrow={1} whiteSpace="pre-wrap">
                {props.message}
            </Box>
        </HStack>
    );
};

export interface LogListProps {
    children: LogListItemProps[];
}

export const LogList: FC<LogListProps & BoxProps> = props => {
    const endPos = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const {current: endPosCurr} = endPos;
        if (!endPosCurr) return;
        endPosCurr.scrollIntoView();
    }, [props.children, endPos.current]);

    return (
        <Box {...props}>
            <Center py={2} opacity={0.8}>
                <Text>Start of logs</Text>
            </Center>

            {props.children.map((item, i) => (
                <LogListItem {...item} key={i} />
            ))}

            <div ref={endPos} />
        </Box>
    );
};
