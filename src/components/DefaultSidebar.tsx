import React, {FC} from "react";
import {Sidebar, SidebarButton, SidebarHeader} from "./Sidebar";

export interface DefaultSidebarProps {
    // path to current page
    current: string;
}

export const DefaultSidebar: FC<DefaultSidebarProps> = () => (
    <Sidebar title="ZoneONE Dashboard">
        <SidebarButton href="/">Home</SidebarButton>
        <SidebarHeader>Monitoring</SidebarHeader>
        <SidebarButton href="/logs">Logs</SidebarButton>
        <SidebarButton href="/request-graphs">Request Graphs</SidebarButton>
    </Sidebar>
);
