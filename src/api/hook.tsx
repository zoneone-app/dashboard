import {
    HubConnection,
    HubConnectionBuilder,
    HubConnectionState
} from "@microsoft/signalr";
import {useEffect, useState} from "react";

const connections = new Map<string, HubConnection>();

function getOrCreateConnection(url: string): HubConnection | null {
    if (typeof window === "undefined") return null;

    if (connections.has(url)) return connections.get(url) as HubConnection;
    const connection = new HubConnectionBuilder().withUrl(url).build();
    connections.set(url, connection);

    return connection;
}

export function useSignalR(
    url: string = process.env.NEXT_PUBLIC_API_URL as string
): HubConnection | null {
    const connection = getOrCreateConnection(url);
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        if (connection?.state === HubConnectionState.Disconnected)
            connection?.start().then(() => setLoaded(true));

        return () => {
            connection?.stop().then(() => setLoaded(false));
        };
    }, [connection]);

    return loaded ? connection : null;
}
